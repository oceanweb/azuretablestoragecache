﻿using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using OceanWebSystems.Extensions.Caching.AzureTableStorage.Sample.Models;

namespace OceanWebSystems.Extensions.Caching.AzureTableStorage.Sample.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDistributedCache _cacheMechanism;

        public HomeController(IDistributedCache cacheMechanism)
        {
            _cacheMechanism = cacheMechanism;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SetValue(CacheItemViewModel viewModel)
        {
            byte[] value = Encoding.UTF32.GetBytes(viewModel.Value);
            await SetCacheValueAsync(viewModel.Key, value);

            ViewBag.Message = $"Item with key '{viewModel.Key}' and value '{viewModel.Value}' added to the cache.";

            return View("Index");
        }

        [HttpPost]
        public async Task<IActionResult> GetValue(CacheItemViewModel viewModel)
        {
            byte[] value = await GetCacheValueAsync(viewModel.Key);
            if (value != null)
            {
                viewModel.Value = Encoding.UTF32.GetString(value);
            }

            ViewBag.Message = $"Item with key '{viewModel.Key}' and value '{viewModel.Value}' retrieved from the cache.";

            return View("Index");
        }

        private Task SetCacheValueAsync(string key, byte[] value)
        {
            return _cacheMechanism.SetAsync(key, value);
        }

        private Task<byte[]> GetCacheValueAsync(string key)
        {
            return _cacheMechanism.GetAsync(key);
        }
    }
}
