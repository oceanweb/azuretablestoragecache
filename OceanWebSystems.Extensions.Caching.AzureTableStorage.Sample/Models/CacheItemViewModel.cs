﻿namespace OceanWebSystems.Extensions.Caching.AzureTableStorage.Sample.Models
{
    public class CacheItemViewModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
