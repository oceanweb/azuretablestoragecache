﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace OceanWebSystems.Extensions.Caching.AzureTableStorage
{
    /// <summary>
    /// An <see cref="IDistributedCache"/> implementation to cache data in Azure table storage.
    /// </summary>
    /// <seealso cref="IDistributedCache"/>.
    public class AzureTableStorageCache : IDistributedCache
    {
        /// <summary>
        /// The storage account connection string.
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// The storage account key.
        /// </summary>
        private readonly string _accountKey;

        /// <summary>
        /// The storage account name.
        /// </summary>
        private readonly string _accountName;

        /// <summary>
        /// The storage table name.
        /// </summary>
        private readonly string _tableName;

        /// <summary>
        /// The storage table partition key.
        /// </summary>
        private readonly string _partitionKey;

        /// <summary>
        /// The storage table client.
        /// </summary>
        private CloudTableClient _tableClient;

        /// <summary>
        /// The storage table.
        /// </summary>
        private CloudTable _table;

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureTableStorageCache"/> class.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string of the storage account.
        /// </param>
        /// <param name="tableName">
        /// The name of the table to use. If the table doesn't exist it will be created.
        /// </param>
        /// <param name="partitionKey">
        /// The partition key to use.
        /// </param>
        public AzureTableStorageCache(string connectionString, string tableName, string partitionKey)
            : this(tableName, partitionKey)
        {
            Error.ArgumentNotNullOrEmpty(connectionString, nameof(connectionString));

            _connectionString = connectionString;
            Connect();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureTableStorageCache"/> class.
        /// </summary>
        /// <param name="accountName">
        /// The name of the storage account.
        /// </param>
        /// <param name="accountKey">
        /// The key of the storage account.
        /// </param>
        /// <param name="tableName">
        /// The name of the table to use. If the table doesn't exist it will be created.
        /// </param>
        /// <param name="partitionKey">
        /// The partition key to use.
        /// </param>
        public AzureTableStorageCache(string accountName, string accountKey, string tableName, string partitionKey)
            : this(tableName, partitionKey)
        {
            Error.ArgumentNotNullOrEmpty(accountName, nameof(accountName));
            Error.ArgumentNotNullOrEmpty(accountKey, nameof(accountKey));

            _accountName = accountName;
            _accountKey = accountKey;
            Connect();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureTableStorageCache"/> class.
        /// </summary>
        /// <param name="tableName">
        /// The name of the table to use. If the table doesn't exist it will be created.
        /// </param>
        /// <param name="partitionKey">
        /// The partition key to use.
        /// </param>
        private AzureTableStorageCache(string tableName, string partitionKey)
        {
            Error.ArgumentNotNullOrEmpty(tableName, nameof(tableName));
            Error.ArgumentNotNullOrEmpty(partitionKey, nameof(partitionKey));

            _tableName = tableName;
            _partitionKey = partitionKey;
        }

        /// <summary>
        /// Gets a value with the given key.
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        /// <returns>
        /// A <see cref="byte[]"/>.
        /// </returns>
        public byte[] Get(string key)
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            return GetAsync(key).Result;
        }

        /// <summary>
        /// Gets a value with the given key.
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        /// <param name="token">
        /// Optional: The <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// A <see cref="byte[]"/>.
        /// </returns>
        public async Task<byte[]> GetAsync(string key, CancellationToken token = default(CancellationToken))
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            await RefreshAsync(key);

            CachedItem item = await RetrieveAsync(key);
            return item?.Data;
        }

        /// <summary>
        /// Refreshes a value in the cache based on its key, resetting its sliding expiration
        /// timeout (if any).
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        public void Refresh(string key)
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            RefreshAsync(key).Wait();
        }

        /// <summary>
        /// Refreshes a value in the cache based on its key, resetting its sliding expiration
        /// timeout (if any).
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        /// <param name="token">
        /// Optional: The <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        public async Task RefreshAsync(string key, CancellationToken token = default(CancellationToken))
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            CachedItem item = await RetrieveAsync(key);
            if (item != null)
            {
                if (ShouldDelete(item))
                {
                    await RemoveAsync(item);
                    return;
                }

                // In theory setting the ETag shouldn't be required but we end up
                // with 'Precondition Failed' errors otherwise.
                item.ETag = "*";
                item.LastAccessTime = DateTimeOffset.UtcNow;

                TableOperation operation = TableOperation.Replace(item);
                await _table.ExecuteAsync(operation);
            }
        }

        /// <summary>
        /// Removes the value with the given key.
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        public void Remove(string key)
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            RemoveAsync(key).Wait();
        }

        /// <summary>
        /// Removes the value with the given key.
        /// </summary>
        /// <param name="key">
        /// A string identifying the requested value.
        /// </param>
        /// <param name="token">
        /// Optional: The <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        public Task RemoveAsync(string key, CancellationToken token = default(CancellationToken))
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));

            CachedItem item = RetrieveAsync(key).Result;
            if (item != null)
            {
                TableOperation operation = TableOperation.Delete(item);
                return _table.ExecuteAsync(operation);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Removes the given item.
        /// </summary>
        /// <param name="item">
        /// The <see cref="CachedItem"/>.
        /// </param>
        /// <param name="token">
        /// Optional: The <see cref="CancellationToken"/>.
        /// </param>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        public Task RemoveAsync(CachedItem item, CancellationToken token = default(CancellationToken))
        {
            TableOperation operation = TableOperation.Delete(item);
            return _table.ExecuteAsync(operation);
        }

        /// <summary>
        /// Sets a value with the given key.
        /// </summary>
        /// <param name="key">
        /// Sets a value with the given key.
        /// </param>
        /// <param name="value">
        /// The value to set in the cache.
        /// </param>
        /// <param name="options">
        /// The cache options for the value.
        /// </param>
        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));
            Error.ArgumentNotNullOrEmpty(value, nameof(value));

            SetAsync(key, value, options).Wait();
        }

        /// <summary>
        /// Sets a value with the given key.
        /// </summary>
        /// <param name="key">
        /// Sets a value with the given key.
        /// </param>
        /// <param name="value">
        /// The value to set in the cache.
        /// </param>
        /// <param name="options">
        /// The cache options for the value.
        /// </param>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default(CancellationToken))
        {
            Error.ArgumentNotNullOrEmpty(key, nameof(key));
            Error.ArgumentNotNullOrEmpty(value, nameof(value));

            DateTimeOffset? absoluteExpiration = null;
            DateTimeOffset currentTime = DateTimeOffset.UtcNow;

            if (options.AbsoluteExpirationRelativeToNow.HasValue)
            {
                absoluteExpiration = currentTime.Add(options.AbsoluteExpirationRelativeToNow.Value);
            }
            else if (options.AbsoluteExpiration.HasValue)
            {
                if (options.AbsoluteExpiration.Value <= currentTime)
                {
                    throw new ArgumentOutOfRangeException(
                       nameof(options.AbsoluteExpiration),
                       options.AbsoluteExpiration.Value,
                       "The absolute expiration value must be in the future.");
                }
                absoluteExpiration = options.AbsoluteExpiration;
            }

            CachedItem item = new CachedItem(_partitionKey, key, value)
            {
                LastAccessTime = currentTime
            };

            if (absoluteExpiration.HasValue)
            {
                item.AbsoluteExpiration = absoluteExpiration;
            }

            if (options.SlidingExpiration.HasValue)
            {
                item.SlidingExpiration = options.SlidingExpiration;
            }

            TableOperation operation = TableOperation.InsertOrReplace(item);
            return _table.ExecuteAsync(operation);
        }

        /// <summary>
        /// Connect to the Azure storage account and gets the table reference.
        /// </summary>
        private void Connect()
        {
            ConnectAsync().Wait();
        }

        /// <summary>
        /// Connect to the Azure storage account and gets the table reference.
        /// </summary>
        /// <returns>
        /// A <see cref="Task"/>.
        /// </returns>
        private async Task ConnectAsync()
        {
            if (_tableClient == null)
            {
                if (string.IsNullOrWhiteSpace(_connectionString))
                {
                    StorageCredentials credentials = new StorageCredentials(_accountKey, _accountKey);
                    _tableClient = new CloudStorageAccount(credentials, true).CreateCloudTableClient();
                }
                else
                {
                    _tableClient = CloudStorageAccount.Parse(_connectionString).CreateCloudTableClient();
                }
            }
            if (_table == null)
            {
                _table = _tableClient.GetTableReference(_tableName);
                if (!await _table.ExistsAsync())
                {
                    await _table.CreateIfNotExistsAsync();
                }                
            }
        }

        /// <summary>
        /// A <see cref="Task"/>.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>
        /// A <see cref="Task{CachedItem}"/>.
        /// </returns>
        private async Task<CachedItem> RetrieveAsync(string key)
        {
            TableOperation operation = TableOperation.Retrieve<CachedItem>(_partitionKey, key);
            TableResult result = await _table.ExecuteAsync(operation);
            CachedItem data = result?.Result as CachedItem;
            return data;
        }

        /// <summary>
        /// Checks whether the cached item should be deleted based on the absolute or sliding expiration values.
        /// </summary>
        /// <param name="item">
        /// The <see cref="CachedItem"/>.
        /// </param>
        /// <returns>
        /// <c>true</c> if the item should be deleted, <c>false</c> otherwise.
        /// </returns>
        private bool ShouldDelete(CachedItem item)
        {
            DateTimeOffset currentTime = DateTimeOffset.UtcNow;
            if (item.AbsoluteExpiration != null &&
                item.AbsoluteExpiration.Value <= currentTime)
            {
                return true;
            }

            if (item.SlidingExpiration.HasValue &&
                item.LastAccessTime.HasValue &&
                item.LastAccessTime.Value.Add(item.SlidingExpiration.Value) < currentTime)
            {
                return true;
            }

            return false;
        }
    }
}
