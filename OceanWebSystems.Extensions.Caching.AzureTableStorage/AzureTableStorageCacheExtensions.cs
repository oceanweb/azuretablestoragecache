﻿using Microsoft.Extensions.Caching.Distributed;
using OceanWebSystems;
using OceanWebSystems.Extensions.Caching.AzureTableStorage;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Extension methods to add Azure table storage cache.
    /// </summary>
    public static class AzureTableStorageCacheExtensions
    {
        /// <summary>
        /// Add Azure table storage cache as an IDistributedCache to the service container.
        /// </summary>
        /// <param name="services">
        /// The <see cref="IServiceCollection"/>.
        /// </param>
        /// <param name="options">
        /// The <see cref="AzureTableStorageCacheOptions"/>.
        /// </param>
        /// <returns>
        /// The updated <see cref="IServiceCollection"/>.
        /// </returns>
        public static IServiceCollection AddDistributedAzureTableStorageCache(
            this IServiceCollection services,
            AzureTableStorageCacheOptions options)
        {
            return AddDistributedAzureTableStorageCache(
                services,
                options.ConnectionString,
                options.TableName,
                options.PartitionKey);
        }

        /// <summary>
        /// Add Azure table storage cache as an IDistributedCache to the service container.
        /// </summary>
        /// <param name="services">
        /// The <see cref="IServiceCollection"/>.
        /// </param>
        /// <param name="connectionString">
        /// The connection string of the storage account.
        /// </param>
        /// <param name="tableName">
        /// The name of the table to use. If the table doesn't exist it will be created.
        /// </param>
        /// <param name="partitionKey">
        /// The partition key to use.
        /// </param>
        /// <returns>
        /// The updated <see cref="IServiceCollection"/>.
        /// </returns>
        public static IServiceCollection AddDistributedAzureTableStorageCache(
            this IServiceCollection services,
            string connectionString,
            string tableName,
            string partitionKey)
        {
            Error.ArgumentNotNullOrEmpty(connectionString, nameof(connectionString));

            services.Add(
                ServiceDescriptor.Singleton<IDistributedCache,
                AzureTableStorageCache>(a => new AzureTableStorageCache(connectionString, tableName, partitionKey)));

            return services;
        }

        /// <summary>
        /// Add Azure table storage cache as an IDistributedCache to the service container.
        /// </summary>
        /// <param name="services">
        /// The <see cref="IServiceCollection"/>.
        /// </param>
        /// <param name="accountName">
        /// The name of the storage account.
        /// </param>
        /// <param name="accountKey">
        /// The key of the storage account.
        /// </param>
        /// <param name="tableName">
        /// The name of the table to use. If the table doesn't exist it will be created.
        /// </param>
        /// <param name="partitionKey">
        /// The partition key to use.
        /// </param>
        /// <returns>
        /// The updated <see cref="IServiceCollection"/>.
        /// </returns>
        public static IServiceCollection AddDistributedAzureTableStorageCache(
            this IServiceCollection services,
            string accountName,
            string accountKey,
            string tableName,
            string partitionKey)
        {
            Error.ArgumentNotNullOrEmpty(accountName, nameof(accountName));
            Error.ArgumentNotNullOrEmpty(accountKey, nameof(accountKey));
            Error.ArgumentNotNullOrEmpty(tableName, nameof(tableName));
            Error.ArgumentNotNullOrEmpty(partitionKey, nameof(partitionKey));

            services.Add(
                ServiceDescriptor.Singleton<IDistributedCache,
                AzureTableStorageCache>(a => new AzureTableStorageCache(accountName, accountKey, tableName, partitionKey)));
            return services;
        }
    }
}
