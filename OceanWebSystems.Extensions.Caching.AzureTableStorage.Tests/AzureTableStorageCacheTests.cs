using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace OceanWebSystems.Extensions.Caching.AzureTableStorage.Tests
{
    public class AzureTableStorageCacheTests
    {
        private readonly IDistributedCache cache;

        public AzureTableStorageCacheTests()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .AddUserSecrets<AzureTableStorageCacheTests>();

            IConfiguration configuration = builder.Build();

            string connectionString = configuration["AzureTableStorageCache:ConnectionString"];
            string tableName = configuration["AzureTableStorageCache:TableName"];
            string partitionKey = configuration["AzureTableStorageCache:PartitionKey"];

            cache = new AzureTableStorageCache(connectionString, tableName, partitionKey);
            cache.Remove("key1");
        }

        [Fact]
        public void Set_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;
            byte[] value = Encoding.UTF32.GetBytes("value1");

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => cache.Set(key, value));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public async Task SetAsync_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;
            byte[] value = Encoding.UTF32.GetBytes("value1");

            // Act
            Exception ex = await Assert.ThrowsAsync<ArgumentNullException>(async () => await cache.SetAsync(key, value));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public void Set_ValueIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = "key1";
            byte[] value = null;

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => cache.Set(key, value));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: value", ex.Message);
        }

        [Fact]
        public async Task SetAsync_ValueIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = "key1";
            byte[] value = null;

            // Act
            Exception ex = await Assert.ThrowsAsync<ArgumentNullException>(async () => await cache.SetAsync(key, value));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: value", ex.Message);
        }

        [Fact]
        public void Set_AbsoluteExpirationIsBeforeNow_ThrowsArgumentOutOfRangeException()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Today.AddDays(-1)
            };

            // Act
            Exception ex = Assert.Throws<ArgumentOutOfRangeException>(() => cache.Set(key, value, options));

            // Assert
            Assert.StartsWith("The absolute expiration value must be in the future.", ex.Message);
            Assert.Contains("Parameter name: AbsoluteExpiration", ex.Message);
        }

        [Fact]
        public async Task SetAsync_AbsoluteExpirationIsBeforeNow_ThrowsArgumentOutOfRangeException()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.Today.AddDays(-1)
            };

            // Act
            Exception ex = await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await cache.SetAsync(key, value, options));

            // Assert
            Assert.StartsWith("The absolute expiration value must be in the future.", ex.Message);
            Assert.Contains("Parameter name: AbsoluteExpiration", ex.Message);
        }

        [Fact]
        public void Set_KeyIsNotNull_ValueIsStored()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            // Act
            cache.Set(key, value);

            // Assert
            byte[] cachedValue = cache.Get(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public async Task SetAsync_KeyIsNotNull_ValueIsStored()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            // Act
            await cache.SetAsync(key, value);

            // Assert
            byte[] cachedValue = await cache.GetAsync(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public void Get_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange/Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => cache.Get(null));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public async Task GetAsync_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange/Act
            Exception ex = await Assert.ThrowsAsync<ArgumentNullException>(async () => await cache.GetAsync(null));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public void Get_ItemDoesNotExist_ValueIsNull()
        {
            // Arrange
            string key = "key1";

            // Act
            byte[] cachedValue = cache.Get(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task GetAsync_ItemDoesNotExist_ValueIsNull()
        {
            // Arrange
            string key = "key1";

            // Act
            byte[] cachedValue = await cache.GetAsync(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public void Get_ItemExists_ValueIsRetrieved()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");
            cache.Set(key, value);

            // Act
            byte[] cachedValue = cache.Get(key);

            // Assert
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public async Task GetAsync_ItemExists_ValueIsRetrieved()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");
            await cache.SetAsync(key, value);

            // Act
            byte[] cachedValue = await cache.GetAsync(key);

            // Assert
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public void Get_ItemExists_AbsoluteExpirationHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.UtcNow.AddMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = cache.Get(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task GetAsync_ItemExists_AbsoluteExpirationHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.UtcNow.AddMilliseconds(1000)
            };

            await cache.SetAsync(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = await cache.GetAsync(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public void Get_ItemExists_AbsoluteExpirationAbsoluteExpirationRelativeToNowHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = cache.Get(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task GetAsync_ItemExists_AbsoluteExpirationAbsoluteExpirationRelativeToNowHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMilliseconds(1000)
            };

            await cache.SetAsync(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = await cache.GetAsync(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public void Get_ItemExists_SlidingExpirationHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = cache.Get(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task GetAsync_ItemExists_SlidingExpirationHasExpired_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromMilliseconds(1000)
            };

            await cache.SetAsync(key, value, options);

            Thread.Sleep(2000);

            // Act
            byte[] cachedValue = await cache.GetAsync(key);

            // Assert
            Assert.Null(cachedValue);
        }

        [Fact]
        public void Set_ItemWithKeyExists_ValueIsUpdated()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");
            cache.Set(key, value);
            value = Encoding.UTF32.GetBytes("value2");

            // Act
            cache.Set(key, value);

            // Assert
            byte[] cachedValue = cache.Get(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public async Task SetAsync_ItemWithKeyExists_ValueIsUpdated()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");
            cache.Set(key, value);
            value = Encoding.UTF32.GetBytes("value2");

            // Act
            await cache.SetAsync(key, value);

            // Assert
            byte[] cachedValue = await cache.GetAsync(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public void Refresh_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => cache.Refresh(key));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public async Task RefreshAsync_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;

            // Act
            Exception ex = await Assert.ThrowsAsync<ArgumentNullException>(async () => await cache.RefreshAsync(key));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public void Refresh_ValueIsRetrievedAfterAbsoluteExpirationHasExpired()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.UtcNow.AddMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(750);

            // Act
            cache.Refresh(key);

            // Assert
            Thread.Sleep(750);
            byte[] cachedValue = cache.Get(key);
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task RefreshAsync_ValueIsRetrievedAfterAbsoluteExpirationHasExpired()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.UtcNow.AddMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(750);

            // Act
            await cache.RefreshAsync(key);

            // Assert
            Thread.Sleep(750);
            byte[] cachedValue = await cache.GetAsync(key);
            Assert.Null(cachedValue);
        }

        [Fact]
        public void Refresh_ValueIsRetrievedAfterOriginalSlidingExpirationHasExpired()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(750);

            // Act
            cache.Refresh(key);

            // Assert
            Thread.Sleep(750);
            byte[] cachedValue = cache.Get(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public async Task RefreshAsync_ValueIsRetrievedAfterOriginalSlidingExpirationHasExpired()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromMilliseconds(1000)
            };

            cache.Set(key, value, options);

            Thread.Sleep(750);

            // Act
            await cache.RefreshAsync(key);

            // Assert
            Thread.Sleep(750);
            byte[] cachedValue = await cache.GetAsync(key);
            Assert.Equal(value, cachedValue);
        }

        [Fact]
        public void Remove_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => cache.Remove(key));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public async Task RemoveAsync_KeyIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            string key = null;

            // Act
            Exception ex = await Assert.ThrowsAsync<ArgumentNullException>(async () => await cache.RemoveAsync(key));

            // Assert
            Assert.StartsWith("Value cannot be null.", ex.Message);
            Assert.Contains("Parameter name: key", ex.Message);
        }

        [Fact]
        public void Remove_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");
            cache.Set(key, value);

            // Act
            cache.Remove(key);

            // Assert
            byte[] cachedValue = cache.Get(key);
            Assert.Null(cachedValue);
        }

        [Fact]
        public async Task RemoveAsync_ValueIsNull()
        {
            // Arrange
            string key = "key1";
            byte[] value = Encoding.UTF32.GetBytes("value1");

            cache.Set(key, value);

            // Act
            await cache.RemoveAsync(key);

            // Assert
            byte[] cachedValue = await cache.GetAsync(key);
            Assert.Null(cachedValue);
        }
    }
}
